<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://xsltsl.org/string" version="1.0">
  
  <xsl:output method="html"/>
  
  <xsl:template match="recette">

  <html>
    <head>
     <title>Recette</title>
    </head>
    <body>
        <h1><xsl:value-of select="nom"/></h1>
        <h2>Ingrédients</h2>
        <ul>
        <xsl:for-each select="ingredients/ingredient">
            <xsl:if test="lang='fr'">
                <li><xsl:value-of select="quantité"/> de <xsl:value-of select="nom"/>
                    (<xsl:value-of select="@blub"/>)
                </li>
            </xsl:if>
        </xsl:for-each>
        </ul>

        <h2>Ingrédients (II)</h2>
        <ul>
        <xsl:for-each select="ingredients/ingredient[@lang='fr']">
                <li><xsl:value-of select="quantité"/> de <xsl:value-of select="nom"/>
                    (<xsl:value-of select="@blub"/>)
                </li>
        </xsl:for-each>
        </ul>

        <h2>Ingrédients (III)</h2>
        <ul>
        <xsl:for-each select="ingredients/ingredient">
            <li><xsl:value-of select="."/></li>
        </xsl:for-each>
        </ul>

        <h2>Étapes</h2>
        <ul>
            <xsl:for-each select="étapes/afaire">
                <li><xsl:value-of select="."/></li>
        </xsl:for-each>
        </ul>
    </body>
  </html>
  </xsl:template>
  
  <!-- Ignore anything else -->
  <xsl:template match="*"></xsl:template>
  
</xsl:stylesheet>



