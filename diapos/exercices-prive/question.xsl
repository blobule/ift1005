<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://xsltsl.org/string" version="1.0">
  
  <xsl:output method="html" indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/studium">
        <h1><xsl:value-of select="cours"/></h1>
        <xsl:apply-templates select="étudiant"/>
  </xsl:template>

  <xsl:template match="étudiant">
      <p><b><xsl:value-of select="id"/></b>:
      <xsl:for-each select="travail">
          <xsl:value-of select="nom"/>=<xsl:value-of select="note"/>,
      </xsl:for-each>
      </p>
  </xsl:template>

</xsl:stylesheet>



