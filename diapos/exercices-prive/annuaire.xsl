<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://xsltsl.org/string" version="1.0">
  
  <xsl:output method="html" indent="yes"/>
  <xsl:strip-space elements="*"/>


  <!--                  -->
  <!-- Page web entière -->
  <!--                  -->

  <xsl:template match="/annuaire">
      <html>
        <head>
            <title><xsl:value-of select="titre"/></title>
            <style>
                body { background-color: #eeeeee; }
                .bottin { display: flex;flex-wrap: wrap; gap: 40px; justify-content:center;
                    font-family: Sans;
                    margin: 20px;
                }
                .information  { margin:20px; }
                .contact {
                    flex:0 0 32%;
                    border: 1px solid black;
                    padding: 5px;
                    display: grid;
                    grid-template-columns: 50% 50%;
                    grid-gap: 0px;
                    box-shadow: 10px 10px 15px 5px #00000080;
                    background-color: white;
                }
                .contact div  {
                    margin:2px;
                    padding: 5px;
                /*background-color:orange;*/
                }
                .nom {  text-transform: uppercase;font-weight:bold; }
                .unite { grid-column: 1 / 3; }

                .labo {
                    text-transform: uppercase;font-weight:bold;
                    text-align:right;
                    align-self: end;
                }
                div.labo, div.labologo { padding: 10px 5px 0px 5px; }
                .labologo {
                    align-self: end;
                }
                div.udemlogonoir {
                    padding:0 0 10px 5px;
                    margin:0;
                }
                div.udemlogobleu {
                    padding-top:10px;
                    margin:0;
                }

                .perso {
                    display:flex;
                    flex-direction: column;
                    justify-content: space-between;
                    gap: 10px;
                }
                .perso div  { /*background-color:purple*/;padding:0px;margin:0px; }
                .adresse  {
                    display:flex;
                    flex-direction: column;
                    justify-content: space-between;
                    gap: 10px;
                }
                .adresse div  { /*background-color:orange;*/padding:0px;margin:0px; }
            </style>
        </head>
        <body>
            <h1><xsl:value-of select="titre"/></h1>
            <div class="information">
                <xsl:value-of select="information"/>
            </div>
            <div class="bottin">
                <xsl:apply-templates select="contact"/>
            </div>
        </body>
      </html>
  </xsl:template>

  <!--                -->
  <!-- Chaque contact -->
  <!--                -->
  <xsl:template match="contact">
      <div class="contact">
        <xsl:if test="labo and logo">
            <div class="labo">
              <xsl:apply-templates select="labo"/>
            </div>
          <div class="labologo">
              <img width="40%">
                  <xsl:attribute name="src">
                      <xsl:value-of select="concat('./logos/',logo,'.png')"/>
                  </xsl:attribute>
              </img>
          </div>
          <div></div>
          <div class="udemlogonoir"><img width="58%" src="udem-logo-noir.svg"/></div>
        </xsl:if>
        <xsl:if test="unite and sous-unite">
          <div></div>
          <div class="udemlogobleu"><img width="70%" src="udem-logo.svg"/></div>
          <div class="unite">
              <b><xsl:apply-templates select="sous-unite"/></b>
              <xsl:value-of select="unite"/>
          </div>
        </xsl:if>
          <div class="adresse">
              <div>
              <xsl:apply-templates select="localisation"/>
              </div>
              <div class="postale">
              <xsl:value-of select="postale"/>
              </div>
          </div>
          <div class="perso">
              <div>
              <span class="nom">
              <xsl:value-of select="prenom"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="nom"/></span><br/>
              <xsl:for-each select="fonction">
                <xsl:value-of select="."/><br/>
              </xsl:for-each>
              </div>
              <div>
              <xsl:for-each select="tel">
                  T <xsl:value-of select="."/><br/>
              </xsl:for-each>
              <xsl:for-each select="cell">
                  C <xsl:value-of select="."/><br/>
              </xsl:for-each>
              </div>
              <xsl:for-each select="courriel">
                  <div>
                      <xsl:value-of  select="."/><br/>
                  </div>
              </xsl:for-each>
              <xsl:for-each select="web">
                  <div>
                  <xsl:value-of  select="."/>
                  </div>
              </xsl:for-each>
          </div>
      </div>
  </xsl:template>

  
  <!-- Ignore tout autre balise -->
  <!--<xsl:template match="*"></xsl:template>-->

  <xsl:template  match="L">
      <xsl:value-of select="."/><br/>
  </xsl:template>
  
</xsl:stylesheet>



