<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://xsltsl.org/string" version="1.0">
  
  <xsl:output method="html"/>
  
  <xsl:template match="current_observation">

  <html>
    <head>
     <title>National Weather Service : Latest Observation for : <xsl:value-of select="location/text()"/> </title>
     <style>
        p { color: blue; }
     </style>
    </head>
    <body>
        <h1><xsl:value-of select="location"/></h1>
        <p>Il fait <xsl:value-of select="temp_c"/> celcius.</p>
        <p>Il fait aussi <xsl:value-of select="temp_f"/> far....</p>
        <p>Il fait <xsl:value-of select="relative_humidity"/> % d' humidité.</p>
    </body>
  </html>
  </xsl:template>
  
  <!-- Ignore anything else -->
  <xsl:template match="*"></xsl:template>
  
</xsl:stylesheet>



