<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://xsltsl.org/string" version="1.0">
  
  <xsl:output method="html"/>
  
  <xsl:template match="ARTICLE">

  <html>
    <head>
     <title><xsl:value-of select="HEADLINE"/> </title>
    </head>
    <style>
        table, th, td { border: 1px solid; }
    </style>
    <body>
        <table>
            <tr><th></th><th><xsl:value-of select="HEADLINE"/></th></tr>
            <tr><th>Auteur</th><td><xsl:value-of select="AUTHOR"/></td></tr>
            <tr><th>Info</th><td>
                    <xsl:apply-templates select="PARA/text()|PARA/*"/>
                </td></tr>
        </table>
    </body>
  </html>
  </xsl:template>
  
  <xsl:template match="text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="INSTRUMENT">
    <b><xsl:value-of select="."/></b>
  </xsl:template>


  <!-- Ignore anything else -->
  <xsl:template match="*"></xsl:template>
  
</xsl:stylesheet>



