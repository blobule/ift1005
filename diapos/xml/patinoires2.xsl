<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://xsltsl.org/string" version="1.0">
  
  <xsl:output method="html"/>
  
  <xsl:template match="patinoires">

  <html>
    <head>
     <title>Patinoires <xsl:value-of select="location/text()"/> </title>
    </head>
    <body>
        <h1>Patinoires</h1>
        <ul>
            <xsl:for-each select="patinoire">
                <xsl:sort select="condition"/>
                <li>
                   <xsl:value-of select="nom"/>
                   <ul>
                       <li>ouvert : <xsl:value-of select="ouvert"/> </li>
                       <li>condition : <xsl:value-of select="condition"/> </li>
                   </ul>
               </li>
            </xsl:for-each>
        </ul>
    </body>
  </html>
  </xsl:template>
  
  <!-- Ignore anything else -->
  <xsl:template match="*"></xsl:template>
  
</xsl:stylesheet>



