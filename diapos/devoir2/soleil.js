
// fonctions qui sont  visibles  de l'exterieur

/** On fait une grosse fonction pour cacher nos  fonctions et variables  **/
(function()  {
	'use strict'; // un peu plus de surveillance de notre code....
	
	var svg;
	var oeil1;
	var oeil2;

	function dessineRayon(parent,angle)  {
		let e=document.createElementNS('http://www.w3.org/2000/svg',"line");
		e.setAttribute("x1","40");
		e.setAttribute("y1","0");
		e.setAttribute("x2","60");
		e.setAttribute("y2","0");
		e.setAttribute("transform","rotate("+angle+")");
		parent.appendChild(e);
	}

	function brule(e) {
		// classe  brulant -> pas  brulant
		e.classList.toggle("brulant");
	}

	function regarde(a1,a2) {
		oeil1.setAttribute("transform","rotate("+a1+" -12 -12)");
		oeil2.setAttribute("transform","rotate("+a2+" 12 -12)");
	}

	function  direction(cx,cy,px,py)  { return  Math.atan2(py-cy,px-cx)/Math.PI*180; }

	function mouvement(e)  {
		let k=screenToSVG(svg,e.clientX,e.clientY);
		//console.log("move!!",e.clientX,e.clientY,k.x,k.y);
		let a1=direction(-12,-12,k.x,k.y);
		let a2=direction(12,-12,k.x,k.y);
		regarde(a1,a2);
		let c=document.getElementById("cible");
		c.setAttribute("cx",k.x);
		c.setAttribute("cy",k.y);
	}

	function screenToSVG(svg,screenX, screenY) {
	   var p = svg.createSVGPoint();
	    p.x = screenX;
	    p.y = screenY;
	    return p.matrixTransform(svg.getScreenCTM().inverse());
	}

	function SVGToScreen(svg,svgX, svgY) {
	   var p = svg.createSVGPoint();
	    p.x = svgX;
	    p.y = svgY;
	    return p.matrixTransform(svg.getScreenCTM());
	}

	function init()  {
		svg=document.getElementById("soleil");
		let g=document.createElementNS('http://www.w3.org/2000/svg',"g");
		g.setAttribute("class","rayon");
		svg.appendChild(g);
		let nb=15;
		for(let a=0;a<360;a=a+(360/nb)) dessineRayon(g,a);

		// animation "brulant"
		setInterval(function  ()  { brule(g); },1000  );

		oeil1=document.getElementById("left");
		oeil2=document.getElementById("right");

		let px=-15;
		let py=12;
		let a1=direction(-12,-12,px,py);
		let a2=direction(12,-12,px,py);
		regarde(a1,a2);

		svg.addEventListener("mousemove", mouvement);
	}

	// Executer seulement quand le document est lu au complet!
	if (document.readyState === "loading") {
		// Pas fini? on attend...
		document.addEventListener("DOMContentLoaded", init);
	}else{ init(); } // `DOMContentLoaded` a deja ete fait.  On init tout de suite.

})();











