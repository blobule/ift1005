


(function () {
    'use strict';

var master; // le body
var diapos;  //  contenant des diapos  (#diapos)
var diapo=[]; // toutes les diapos

// info du  css
var  nbc,nbr,diapoW,diapoH,gap;

// etat courant
var current=0;  // diapos actuelle affichee
var single=false;  //  true:  mode single  diapo, false: list tout

    // en  mode single=false, scroll est la position (en pixel diapoY)
    // ratio est le ratio  actuel
var scroll=0;
var range=1;  // nb  de  diapos visible (vertical)
var ratio=1;


///////////////////////////////
// local storage
//////////////////////////////

function localSave(key,info) {
    localStorage.setItem(key+"::"+window.location.pathname, JSON.stringify(info));
}

function localLoad(key,ref) {
    var toto=localStorage.getItem(key+"::"+window.location.pathname);
    if( toto==null )  return ref;
    return JSON.parse(toto);
}

///////////////////////////////////
///////////////////////////////////

// ajuste  la  position actuelle du  scroll

// affiche la diapo n, dans  le mode "single" (true/false)
// la dispo actuelle est  current, et le mode actuel est  single
// 2  cas...
// le mode  change  ou pas,  la diapo change  ou pas
function see(n,mode) {
    console.log("see "+n);
    if(n<0 || n>=diapo.length) return;

    // remove current active,  then add active
    if( current>=0 && current<diapo.length ) {
        diapo[current].classList.remove("active");
    }
    diapo[n].classList.add("active");

    if( mode ) recordDiapo(n); // pour  garder une trace des chapitres

    const { innerWidth, innerHeight } = window;

    console.log("inner w="+innerWidth+" h="+innerHeight);
    console.log("screen w="+screen.width+" h="+screen.height);

    
    // class "single" en monde  single diapo
    if( mode ) master.classList.add("single");
    else master.classList.remove("single");


    //console.log("COL "+col+" ROW "+row);

    var r,offX,tx,ty;

    if( mode )  {
        // Affichage d'une seule diapo!!
        // la  grille est 1 element  par  rangee
        const col=0;
        const row=n;
        // ratios horizontal  et vertical  pour  une seule diapos
        const rw=innerWidth/(diapoW);
        const rh=innerHeight/(diapoH);
        r=(rw<rh)?rw:rh; // le + petit  qui fit dans la fenetre
        offX=(innerWidth-r*diapoW)/2; // centrage
        tx=-col*(diapoW+gap);
        ty=-row*(diapoH+gap);
        window.scrollTo(0,0);
        //window.scrollTo({top:0,left:0,behavior:'smooth'});
    }else{
        // materiel en avant  des diapos
        var extraScroll=0;
        var p=document.getElementsByTagName("header");
        if( p!=null ) {
            p=p[0].getBoundingClientRect();
            extraScroll=(p.bottom-p.top);
        }
        // Affichage de multiples diapos!!
        // la  grille est nbc element  par  rangee
        const col=n%nbc;
        const row=Math.floor(n/nbc);
        r=(innerWidth/(diapoW*nbc+gap*(nbc-1)));
        offX=0; // pas  de centrage en mode multi
        tx=0;
        ty=0;
        // on veut la diapo courante au milieu de le fenetre
        // scroll item doit etre (row+0.5)-range/2
        var newrange=(innerHeight/r)/(diapoH+gap);
        var moveY=((row+0.5)-newrange/2);
        //console.log("scrollY "+moveY +  "row "+row+" range "+newrange);
        moveY=moveY*(diapoH+gap)*r;
        moveY=moveY+extraScroll;
        if( moveY<0 ) moveY=0;
        window.scrollTo(0,moveY);
        //window.scrollTo({top:moveY,left:0,behavior:'smooth'});
        //console.log("scrollY "+moveY);
    }
    // pas quand on print
    diapos.style.transform="translate("+offX+"px,0px) scale("+r+") translate("+tx+"px,"+ty+"px)";

    var dw=(diapoW*nbc+gap*(nbc-1));
    var dh=(diapoH*nbr+gap*(nbr-1));
    //diapos.style.width=(dw*r)+"px";
// pour print
    diapos.style.height=(dh*r)+"px";
    //master.style.width=(dw*r)+"px";
    //master.style.height=(dh*r)+"px";
    console.log("diapos w="+dw+" h="+dh);

    // ajuste en  fonction  des nouvelles tailles
    ratio=r; // pour  info!
    range=(innerHeight/ratio)/(diapoH+gap);
    //document.getElementById("myheader").innerHTML="scroll "+scroll+" range "+range;

    // ajuste l'etat actuel
    current=n;
    single=mode;

    // sauvegarde l'etat, en cas de reload
    localSave("position",{current:current,single:single});

}


/**** auto syntaxe ****/
/** syntax="html" ou  "js"  ou "css", ou  autre...  svg **/
function doAutoSyntax() {
  var e= document.querySelectorAll('[syntax]');
  for(var i=0;i<e.length;i++) {
      console.log("SYNTAX ",e[i].getAttribute("syntax"),e[i]);
      if( typeof w3CodeColor !== 'undefined' ) {
          w3CodeColor(e[i],e[i].getAttribute("syntax"));
      }
  }
}

/**** auto resize des elements "autoreduce" ****/
function doAutoReduce() {
  var boxes=document.getElementsByClassName("autoreduce");
    console.log("boxes ",boxes.length);
  for(var i=0;i<boxes.length;i++) {
    let b=boxes[i];
    let rb=b.getBoundingClientRect();
    let c=b.children[0];
    let rc=c.getBoundingClientRect();
     // console.log("bound "+rb.left+":"+rb.right+ " sz="+(rb.right-rb.left));
     // console.log("bound "+rc.left+":"+rc.right+ " sz="+(rc.right-rc.left));
      // suppose un padding egal de chaque cote
    let target=rb.right-rb.left-(rc.left-rb.left)*2;
    let actual=rc.right-rc.left;
    let ratio=target/actual;
    // console.log("target="+target+" actual="+actual+" ratio "+ratio);
    // suppose une taille font de 1em,  reduit  seulement
    if( ratio<1.0  ) { c.style.fontSize=ratio+"em"; }
  }
}

///////////////////// Timer stuff //////////////////

var tm,tmDisplay,tmReset,tmSave;
var tmInfo=[]; // liste  des evenement diapos...  {time:123.45,diapo:"4 blub"}

var startTime;
function timeStamper(t) {
    var sec=Math.floor(t)%60;
    var min=Math.floor(t/60)%60;
    var hou=Math.floor(t/3600);
    return hou.toString().padStart(2,'0')+":"+min.toString().padStart(2,'0')+":"+sec.toString().padStart(2,'0');
}
function timerClock() {
    var now=new  Date();
    var delta=(now-startTime)/1000;
    tmDisplay.innerHTML=timeStamper(delta);
}

function recordDiapo(n) {
    if(tm==null) return;
    var delta=Math.floor(((new Date())-startTime)/1000);
    if( tmInfo.length>0 && tmInfo[tmInfo.length-1].diapo==n ) return;  // same diapo!
    while(tmInfo.length>0 && (delta-tmInfo[tmInfo.length-1].time)<10 )  {
        // le dernier  n' a pas survecu assez longtemps... on  efface
        tmInfo.pop();
    }
    tmInfo.push({"time":delta,"diapo":n});
    timerSave();
}

function timerSave()  {
    var  t="";
    for(var  i=0;i<tmInfo.length;i++) {
        console.log("    timer "+timeStamper(tmInfo[i].time)+" >>> "+tmInfo[i].diapo);
        var h=diapo[tmInfo[i].diapo].getElementsByTagName("h2");
        t=t+timeStamper(tmInfo[i].time)+" ["+(tmInfo[i].diapo+1)+"]";
        if( h.length>0 ) t=t+" "+h[0].textContent;
        t=t+"\n";
    }
    tmSave.textContent=t;
}

//////////////////////////////////////////////////////

function init() {
  console.info("DOM loaded");
  console.log(window.location.pathname);

  doAutoSyntax();
  doAutoReduce();

  // trouve toutes les slides
  diapos=document.getElementById("diapos");
  diapo = diapos.getElementsByTagName("section");

  console.log(diapo.length+" diapos");

  // trouve le container
  master=document.getElementsByTagName("body")[0];

  //  info du css sur  l' affichage
  var e=getComputedStyle(document.documentElement); 
  nbc=parseInt(e.getPropertyValue('--nbc'),10); 
  diapoW=parseInt(e.getPropertyValue('--diapoW'),10); 
  diapoH=parseInt(e.getPropertyValue('--diapoH'),10); 
  gap=parseInt(e.getPropertyValue('--gap'),10); 
  nbr=Math.ceil(diapo.length/nbc);
  console.log("NBC "+nbc+ "NBR "+nbr+" dW "+diapoW+" dH "+diapoH+" gap "+gap);


    // ajoute a chaque diapo  un  numero  de diapo
 // <div style="font-size:3em;position:absolute;right:30px;bottom:30px;">6</div>

  master.addEventListener("keydown",(event)=>{
      if(event.defaultPrevented) return;
      switch(event.key.toUpperCase()) {
          case 'ARROWLEFT':
              see(current-1,single);
              event.preventDefault();
              break;
          case 'ARROWRIGHT':
              see(current+1,single);
              event.preventDefault();
              break;
          case 'ARROWUP':
              if(!single)see(current-nbc,single);
              event.preventDefault();
              break;
          case 'ARROWDOWN':
              if(!single)see(current+nbc,single);
              event.preventDefault();
              break;
          case 'ENTER' :
          case 'ESCAPE' :
              see(current,!single);
              event.preventDefault();
              break;
      }
  });

  window.addEventListener('resize', ()=>{ see(current,single); });

  window.addEventListener('scroll', ()=>{
      // diapos fractionnaire au top  de l'ecran
      scroll=(window.scrollY/ratio)/(diapoH+gap);
      //document.getElementById("myheader").innerHTML="scroll "+scroll+" range "+range;
  });

  // en mode Multi, un clic passe en mode Single
  for(let i=0;i<diapo.length;i++) {
      diapo[i].addEventListener('click', (e)=>{ if( !single ) see(i,true); });
  }

  // on a deja une position sauvegardee?
  //localStorage.clear();
  var j=localLoad("position",{current:0,single:false});
  current=j.current;
  single=j.single;
  // mode live pose des problemes...
  if( current<0 || current>=diapo.length ) { current=0;single=false; }

  // go!!
  see(current,single);

  // setup  timer,  if  needed...
  tm=document.getElementById("timer");
  tmDisplay=null;
  tmReset=null;
  tmSave=null;
  console.log(tm==null);
  if( tm!=null )  {
    console.log("setup timer!");
    tmDisplay = document.createElement("div");
    tm.appendChild(tmDisplay);
    tmReset = document.createElement("button");
    tm.appendChild(tmReset);
    tmSave = document.createElement("textarea");

    tm.appendChild(tmSave);

    tmDisplay.innerHTML="00:00:00";
    tmReset.innerHTML="Reset";
    tmSave.innerHTML="Save";

    tmReset.addEventListener("click",(e)=>{startTime=new Date();while(tmInfo.length>0) tmInfo.pop();timerClock();timerSave();});
    //tmSave.addEventListener("click",(e)=>{console.log("Save!");timerSave()});

    startTime=new Date();
    setInterval(timerClock,1000);

  }
}

// Executer seulement quand le document est lu au complet!
if (document.readyState === "loading") {
  // Loading hasn't finished yet
  document.addEventListener("DOMContentLoaded", init);
} else { init(); } // `DOMContentLoaded` has already fired

})();

