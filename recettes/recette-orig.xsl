<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://xsltsl.org/string" version="1.1">
  
<xsl:output method="html" indent="yes" />
<xsl:strip-space  elements="*"/>

    <!-- Une seule recette  -->

<xsl:template match="/">
<html>
    <head>
        <link rel="stylesheet" href="recette.css"/>
        <link href="https://fonts.googleapis.com/css2?family=Special+Elite" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Courgette" rel="stylesheet"/>
        <style>
            body  { font-family: "Special Elite", cursive; font-size:1.5rem; line-height:1.4em; }
        </style>
    </head>
    <body>
        <xsl:apply-templates select="//recette"/>
    </body>
</html>
</xsl:template>


<xsl:template match="z">
    <b style="background-color:red;"><xsl:value-of select="."/></b>
</xsl:template>

<xsl:template match="text()">
    <xsl:value-of select="."/>
</xsl:template>


<xsl:template match="q">
    <span class="quantite {./@systeme}">
        <xsl:value-of select="."/> <xsl:value-of select="./@unite"/>
    </span>
</xsl:template>


<xsl:template match="recette">

    <div class="recette card" id="REC-{@id}">
        <div class="titre">
            <h1><xsl:value-of select="nom"/></h1>
        </div>
        <div class="photo">
            <xsl:choose>
                <xsl:when test="image"><img src="{image/@url}" alt="{nom}"/></xsl:when>
                <xsl:otherwise><img class="nophoto" src="icons/restaurant-plate-svgrepo-com.svg"/></xsl:otherwise>
            </xsl:choose>
        </div>
       <div class="informations">
        <!--<h1><xsl:value-of select="nom"/></h1>-->
        <ul>
            <li>Portions <xsl:value-of select="portions"/></li>
            <li>Source <a  href="{source/@url}"> <xsl:value-of select="source"/> </a></li>
            <li>Auteur <a  href="{auteur/@url}"> <xsl:value-of select="auteur"/> </a></li>
            <li>Licence <a  href="{licence/@url}"> <xsl:value-of select="licence"/> </a></li>
            <xsl:for-each select="extra">
                <li><xsl:value-of select="nom"/>
                <xsl:value-of select="val"/></li>
            </xsl:for-each>
        </ul>
        <p><xsl:value-of select="description"/></p>
      </div>

        <!--  par  parties -->
        <!--<xsl:apply-templates select="partie"/>-->
        <!--  par ingredients  et etapes -->
      <div class="ingredients">
        <h2>Ingrédients</h2>
        <xsl:apply-templates select="partie/ingredients"/>
      </div>
      <div class="etapes">
        <h2>Préparation</h2>
        <xsl:apply-templates select="partie/etapes"/>
      </div>
    </div>
</xsl:template>


<xsl:template match="ingredients">
    <xsl:if test="../nom">
        <h3><xsl:value-of select="../nom"/></h3>
    </xsl:if>
    <ul>
        <xsl:apply-templates select="i|ou"/>
    </ul>
</xsl:template>

<xsl:template match="etapes">
    <xsl:if test="../nom">
        <h3><xsl:value-of select="../nom"/></h3>
    </xsl:if>
    <ol start="{count(../preceding-sibling::*//e)+1}">
    <xsl:apply-templates select="e"/>
    </ol>
</xsl:template>

<xsl:template match="ou">
    <li class="ou">
        <ul>
        <xsl:apply-templates select="i"/>
        </ul>
    </li>
</xsl:template>

<!--
        <xsl:for-each select="q">
            <span class="quantite {./@systeme}">
                <xsl:value-of select="."/> <xsl:value-of select="./@unite"/>
            </span>
        </xsl:for-each>
-->

<xsl:template match="i">
    <li><xsl:apply-templates select="./text()|./*"/></li>
</xsl:template>

<xsl:template match="e">
    <li><xsl:apply-templates select="./text()|./*"/></li>
</xsl:template>


  <!-- Ignore anything else -->
    <!--<xsl:template match="*"></xsl:template>-->
  
</xsl:stylesheet>



